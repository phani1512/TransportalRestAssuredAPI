package com.TestCases;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.base.TestBase;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.specification.RequestSpecification;

public class TC_001_GET_loginPage extends TestBase {

	@BeforeClass
	void URLPage() {
		// Base URI
		logger.info("***** Started TC001_Get_URLPage *****");
		RestAssured.baseURI = "https://transportal.azurewebsites.net/";
		RequestSpecification httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET);
	}

	@Test
	void checkResponseBody() {
		logger.info("***** Checking Response Body *****");
		String responseBody = response.getBody().asString();
		logger.info("Response Body ==> " + responseBody);
		Assert.assertTrue(responseBody != null);
	}

	// Status code verification
	@Test
	void checkStatuscode() {
		logger.info("***** Checking Status Code *****");
		int statusCode = response.getStatusCode();
		logger.info("Status Code ==> " + statusCode);
		Assert.assertEquals(statusCode, 200);
	}

	@Test
	// Status line verification
	void checkStatusLine() {
		logger.info("***** Checking Status Line *****");
		String statusLine = response.getStatusLine();
		logger.info("Status Line ==> " + statusLine);
		Assert.assertEquals(statusLine, "HTTP/1.1 200 OK");

	}

	// Response Time Verification
	@Test(enabled = false)
	void checkResponseTime() {
		logger.info("***** Checking Response Time *****");
		long responseTime = response.getTime();
		logger.info("Response Time ==> " + responseTime);
		if (responseTime > 2000)
			logger.warn("Response Time is greater than 2000");
		Assert.assertTrue(responseTime < 2000);

	}

	@Test
	void checkContentType() {
		logger.info("***** Checking Content Type *****");
		String contentType = response.header("Content-Type");
		logger.info("Content Type is ==> " + contentType);
		Assert.assertEquals(contentType, "text/html");

	}

	@Test
	void checkServerType() {
		logger.info("***** Checking Server Type *****");
		String serverType = response.header("Server");
		logger.info("Server Type is ==> " + serverType);
		Assert.assertEquals(serverType, "Microsoft-IIS/10.0");

	}

	@Test
	void checkContentEncoding() {
		logger.info("***** Checking Content Encoding *****");
		String ContentEncoding = response.header("Content-Encoding");
		logger.info("Content Encoding is ==> " + ContentEncoding);
		Assert.assertEquals(ContentEncoding, "gzip");
	}
	@Test
	void checkContentLength() {
		logger.info("***** Checking Content Length *****");
		String ContentLength = response.header("Content-Length");
		logger.info("Content Length is ==> " + ContentLength);
		if(Integer.parseInt(ContentLength)<100)
			logger.warn("Content Length is less than 100");
		Assert.assertTrue(Integer.parseInt(ContentLength)>100);
	}
	
	@AfterClass
	void tearDown() {
		logger.info("***** Finished TC001_Get_URLPage *****");
}
}
